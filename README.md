# Data Preparation

Ce projet sert à documenter dans des issues la préparation des données nécessaire pour la V2 de l'application OMI / mon quartier à pied

## périmètre

L'application OMI va être complètement mise à jour dans le cadre du [projet financé par l'AAP Ademe 'mon quartier à pied'](https://wiki.resilience-territoire.ademe.fr/wiki/Mon_quartier_%C3%A0_pied)

Les principaux points sur les données à produire :
- périmètre France métropolitaine au lieu de PACA
- indicateur "impasses" devient "réseau piéton" avec impasses, impasses traversantes, voies interdites aux voiture...
- indicateur OMI Score
- indicateur population
- indicateur propriétaires publics des parcelles
- indicateurs par commune

## état actuel des notebooks

Pour produire les données, les notebooks suivants doivent être exécutés
- [load-osm-graphs](https://gitlab.com/open-mobility-indicators/indicators/load-osmnx-graphs) convertit des pbf OSM en graphml
- [pedestrian-way-types](https://gitlab.com/open-mobility-indicators/indicators/pedestrian-way-types) lit 2 graphml (réseau piéton + réseau voiture) pour produire la couche des voies piétonnes classées par type (impasses etc.) ; branche graphml pas encore mergée dans main
- [cycles](https://gitlab.com/open-mobility-indicators/indicators/cycles/) branche graphml pour lire graphml et produire un geojson des ilots/cycles
- [population-density-from-cycles](https://gitlab.com/open-mobility-indicators/indicators/population-density-from-cycles/) lit le geojson des ilots du réseau piéton précédent pour produire la couche des ilots piétons habillée par l'indicateur de (densité de) population ; branche blocks avec algo + rapide qui n'utilise pas le parallélisme, à merger dans main.
- [mon-quartier-a-pied](https://gitlab.com/open-mobility-indicators/indicators/mon-quartier-a-pied/) lit le geojson produit par population-density-from-cycles et calcule le "score OMI" à partir de la population et du périmètre de chaque ilot, et produit un nouveau geojson ; branche score-from-cycles pas encore mergée dans main
- [stats-par-commune](https://gitlab.com/open-mobility-indicators/indicators/stats-par-commune) produit un CSV d'indicateurs par commune à partir du geojson produit par mon-quartier-a-pied et des contours des communes : le code est à revoir complètement, pour l'instant c'est codé en dur pour fonctionner sur le 04, à généraliser sur France entière. Un CSV similaire serait à écrire avec en entrée le GeoJSON des types de voirie piétonnes.
- [parcelles-publiques](https://gitlab.com/open-mobility-indicators/indicators/parcelles-publiques/) produit un geoJSON à partir des données dgfip de contours des parcelles et personnes morales propriétaires ; à revoir pour que le code fonctionne proprement sur France entière

## simplification des notebooks au 11/10

Pour produire les données, les notebooks suivants doivent être exécutés:
- [pedestrian-way-types](https://gitlab.com/open-mobility-indicators/indicators/pedestrian-way-types) lit un fichier pbf OSM et produit un eoJSON des voies classées par type (impasses, traverses, etc.) ; le temps de calcul pour une région (données pbf de geofabrik) semble acceptable ; pour Languedoc-Roussillon env 20 minutes et geojson 1.8Go ; branche pyrosm
- [cycles](https://gitlab.com/open-mobility-indicators/indicators/cycles/) lit un fichier pbf OSM et produit un GeoJSON des géométries des ilots ; le temps de calcul pour une région (données pbf de geofabrik) semble acceptable ; pour Languedoc-Roussillon env 10 minutes ; branche pyrosm
- [population-density-from-cycles](https://gitlab.com/open-mobility-indicators/indicators/population-density-from-cycles/) lit le geojson des ilots du réseau piéton précédent pour produire la couche des ilots piétons habillée par l'indicateur de (densité de) population ; branche blocks avec algo + rapide qui n'utilise pas le parallélisme, à merger dans main.
- [mon-quartier-a-pied](https://gitlab.com/open-mobility-indicators/indicators/mon-quartier-a-pied/) lit le geojson produit par population-density-from-cycles et calcule le "score OMI" à partir de la population et du périmètre de chaque ilot, et produit un nouveau geojson ; branche score-from-cycles pas encore mergée dans main
- [stats-par-commune](https://gitlab.com/open-mobility-indicators/indicators/stats-par-commune) produit un CSV d'indicateurs par commune à partir du geojson produit par mon-quartier-a-pied et des contours des communes : le code est à revoir complètement, pour l'instant c'est codé en dur pour fonctionner sur le 04, à généraliser sur France entière. Un CSV similaire serait à écrire avec en entrée le GeoJSON des types de voirie piétonnes.
- [parcelles-publiques](https://gitlab.com/open-mobility-indicators/indicators/parcelles-publiques/) produit un geoJSON à partir des données dgfip de contours des parcelles et personnes morales propriétaires ; à revoir pour que le code fonctionne proprement sur France entière


Le 1er objectif est de faire tourner tous ces notebooks pour le département test de l'Hérault.
Le 2ème objectif est de trouver la solution la plus simple pour faire de même sur la France métropolitaine.
